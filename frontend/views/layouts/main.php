<?php  use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

use frontend\assets\FrontendAsset;
FrontendAsset::register($this);
$this->beginBody() ?>

    <script src="js/react.js"></script>
      <script src="js/react-dom.js" ></script>
      <script src="js/babel.min.js"></script>

     
   
     
<div id="header"> 

<script src="js/header.jsx" type="text/babel"></script>


    
    <?php echo $content ?>
    <footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?php echo date('Y') ?></p>
        <p class="pull-right"><?php echo Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>